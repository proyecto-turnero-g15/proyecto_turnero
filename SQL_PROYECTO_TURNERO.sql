
CREATE TABLE Prioridad (
                id INTEGER NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                codigo VARCHAR(8) NOT NULL,
                CONSTRAINT prioridad_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Prioridad IS 'Datos de Prioridad.';
COMMENT ON COLUMN Prioridad.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Prioridad.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN Prioridad.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN Prioridad.codigo IS 'Valor �nico.';


CREATE TABLE Servicio (
                id INTEGER NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                codigo VARCHAR(8) NOT NULL,
                Prioridad_id INTEGER NOT NULL,
                CONSTRAINT servicio_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Servicio IS 'Datos del Servicio.';
COMMENT ON COLUMN Servicio.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Servicio.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN Servicio.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN Servicio.codigo IS 'Valor �nico.';


CREATE TABLE Departamento_1 (
                id INTEGER NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                codigo VARCHAR(8) NOT NULL,
                CONSTRAINT departamento_1_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Departamento_1 IS 'Datos de Departamento.';
COMMENT ON COLUMN Departamento_1.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Departamento_1.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN Departamento_1.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN Departamento_1.codigo IS 'Valor �nico.';


CREATE TABLE Puesto (
                id INTEGER NOT NULL,
                Dep_id INTEGER NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT puesto_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Puesto IS 'Datos de ubicacion puesto del dep.';
COMMENT ON COLUMN Puesto.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Puesto.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN Puesto.activo IS 'Estado de la entidad.';


CREATE TABLE Ticket (
                id INTEGER NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                Dep_id INTEGER NOT NULL,
                Prioridad_id INTEGER NOT NULL,
                Puesto_id INTEGER NOT NULL,
                codigo VARCHAR(8) NOT NULL,
                Servicio_id INTEGER NOT NULL,
                CONSTRAINT ticket_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Ticket IS 'Datos de Ticket.';
COMMENT ON COLUMN Ticket.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Ticket.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN Ticket.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN Ticket.codigo IS 'Valor �nico.';


CREATE SEQUENCE permiso_id_seq;

CREATE TABLE Permiso (
                id SMALLINT NOT NULL DEFAULT nextval('permiso_id_seq'),
                codigo VARCHAR(8) NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT permiso_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN Permiso.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN Permiso.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Permiso.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN Permiso.activo IS 'Estado de la entidad.';


ALTER SEQUENCE permiso_id_seq OWNED BY Permiso.id;

CREATE SEQUENCE rol_id_seq;

CREATE TABLE Rol (
                id SMALLINT NOT NULL DEFAULT nextval('rol_id_seq'),
                codigo VARCHAR(8) NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT rol_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Rol IS 'Tabla para registrar los roles para los actores del sistema.';
COMMENT ON COLUMN Rol.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN Rol.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Rol.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN Rol.activo IS 'Estado de la entidad.';


ALTER SEQUENCE rol_id_seq OWNED BY Rol.id;

CREATE TABLE Asignacion (
                id SMALLINT NOT NULL,
                permiso_id SMALLINT NOT NULL,
                rol_id SMALLINT NOT NULL,
                CONSTRAINT asignacion_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Asignacion IS 'Tabla intermedio para referenciar roles y permisos.';
COMMENT ON COLUMN Asignacion.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN Asignacion.permiso_id IS 'Identificador de la entidad. Secuencia autogenerada.';


CREATE SEQUENCE pais_id_seq;

CREATE TABLE Pais (
                id SMALLINT NOT NULL DEFAULT nextval('pais_id_seq'),
                codigo_iso2 CHAR(2) NOT NULL,
                codigo_iso3 CHAR(3) NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT pais_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Pais IS 'Tabla param�trica para registrar Paises.';
COMMENT ON COLUMN Pais.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Pais.codigo_iso2 IS 'Valor �nico ISO-2.';
COMMENT ON COLUMN Pais.codigo_iso3 IS 'Valor �nico ISO-3.';
COMMENT ON COLUMN Pais.descripcion IS 'Nombre del pais.';
COMMENT ON COLUMN Pais.activo IS 'Estado de la entidad.';


ALTER SEQUENCE pais_id_seq OWNED BY Pais.id;

CREATE UNIQUE INDEX pais_codigo_uq
 ON Pais USING BTREE
 ( codigo_iso2 ASC, codigo_iso3 ASC );

CREATE SEQUENCE nacionalidad_id_seq;

CREATE TABLE Nacionalidad (
                id SMALLINT NOT NULL DEFAULT nextval('nacionalidad_id_seq'),
                pais_id SMALLINT NOT NULL,
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(24) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT nacionalidad_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Nacionalidad IS 'Tabla param�trica: Nacionalidades.';
COMMENT ON COLUMN Nacionalidad.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Nacionalidad.pais_id IS 'Referencia (fk) a la entidad pais.';
COMMENT ON COLUMN Nacionalidad.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Nacionalidad.descripcion IS 'Nombre de la naciolidad.';
COMMENT ON COLUMN Nacionalidad.activo IS 'Estado de la entidad.';


ALTER SEQUENCE nacionalidad_id_seq OWNED BY Nacionalidad.id;

CREATE UNIQUE INDEX nacionalidad_codigo_uq
 ON Nacionalidad USING BTREE
 ( codigo ASC );

CREATE SEQUENCE sexo_id_seq;

CREATE TABLE Sexo (
                id SMALLINT NOT NULL DEFAULT nextval('sexo_id_seq'),
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT sexo_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Sexo IS 'Tabla param�trica: Sexo.';
COMMENT ON COLUMN Sexo.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN Sexo.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Sexo.descripcion IS 'Nombre de sexo.';
COMMENT ON COLUMN Sexo.activo IS 'Estado de la entidad.';


ALTER SEQUENCE sexo_id_seq OWNED BY Sexo.id;

CREATE UNIQUE INDEX sexo_codigo_uq
 ON Sexo
 ( codigo ASC );

CREATE SEQUENCE tipo_documento_id_seq;

CREATE TABLE Tipo_documento (
                id SMALLINT NOT NULL DEFAULT nextval('tipo_documento_id_seq'),
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT tipo_documento_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Tipo_documento IS 'Tabla param�trica: Tipos de Documentos.';
COMMENT ON COLUMN Tipo_documento.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Tipo_documento.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Tipo_documento.descripcion IS 'Nombre del tipo del tipo de documento.';
COMMENT ON COLUMN Tipo_documento.activo IS 'Estado de la entidad.';


ALTER SEQUENCE tipo_documento_id_seq OWNED BY Tipo_documento.id;

CREATE UNIQUE INDEX tipo_documento_codigo_uq
 ON Tipo_documento
 ( codigo ASC );

CREATE SEQUENCE tipo_persona_id_seq;

CREATE TABLE Tipo_persona (
                id SMALLINT NOT NULL DEFAULT nextval('tipo_persona_id_seq'),
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT tipo_persona_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Tipo_persona IS 'Tabla param�trica: Tipos de Personas.';
COMMENT ON COLUMN Tipo_persona.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Tipo_persona.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Tipo_persona.descripcion IS 'Nombre del tipo de persona.';
COMMENT ON COLUMN Tipo_persona.activo IS 'Estado de la entidad.';


ALTER SEQUENCE tipo_persona_id_seq OWNED BY Tipo_persona.id;

CREATE UNIQUE INDEX tipo_persona_codigo_uq
 ON Tipo_persona USING BTREE
 ( codigo ASC );

CREATE SEQUENCE departamento_id_seq;

CREATE TABLE Departamento (
                id SMALLINT NOT NULL DEFAULT nextval('departamento_id_seq'),
                pais_is SMALLINT NOT NULL,
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT departamento_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Departamento IS 'Tabla param�trica: Departamentos PY.';
COMMENT ON COLUMN Departamento.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Departamento.pais_is IS 'Referencia (fk) a la entidad pais.';
COMMENT ON COLUMN Departamento.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Departamento.descripcion IS 'Nombre del departamento del PY.';
COMMENT ON COLUMN Departamento.activo IS 'Estado de la entidad.';


ALTER SEQUENCE departamento_id_seq OWNED BY Departamento.id;

CREATE UNIQUE INDEX departamento_codigo_iuq
 ON Departamento USING BTREE
 ( codigo ASC );

CREATE SEQUENCE ciudad_id_seq;

CREATE TABLE Ciudad (
                id SMALLINT NOT NULL DEFAULT nextval('ciudad_id_seq'),
                departamento_id SMALLINT NOT NULL,
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT ciudad_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Ciudad IS 'Tabla param�trica: Distritos PY.';
COMMENT ON COLUMN Ciudad.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Ciudad.departamento_id IS 'Referencia (fk) a la entidad departamento.';
COMMENT ON COLUMN Ciudad.codigo IS 'Valor �nico.';
COMMENT ON COLUMN Ciudad.descripcion IS 'Nombre del distrito del PY.';
COMMENT ON COLUMN Ciudad.activo IS 'Estado de la entidad.';


ALTER SEQUENCE ciudad_id_seq OWNED BY Ciudad.id;

CREATE UNIQUE INDEX distrito_codigo_uq
 ON Ciudad USING BTREE
 ( codigo ASC );

CREATE TABLE Persona (
                id INTEGER NOT NULL,
                tipo_persona_id SMALLINT NOT NULL,
                tipo_documento_id SMALLINT NOT NULL,
                numero_documento VARCHAR(16) NOT NULL,
                nombre VARCHAR(64) NOT NULL,
                apellido VARCHAR(64) NOT NULL,
                fecha_nacimiento DATE NOT NULL,
                lugar_nacimiento SMALLINT NOT NULL,
                sexo_id SMALLINT NOT NULL,
                nacionalidad_id SMALLINT NOT NULL,
                direccion VARCHAR(70),
                telefono VARCHAR(20),
                email VARCHAR(50),
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT persona_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Persona IS 'Datos Personales.';
COMMENT ON COLUMN Persona.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Persona.tipo_persona_id IS 'Referencia (fk) a la entidad tipo de persona.';
COMMENT ON COLUMN Persona.tipo_documento_id IS 'Referencia (fk) a la entidad tipo de documento.';
COMMENT ON COLUMN Persona.numero_documento IS 'N�mero de documento de identidad.';
COMMENT ON COLUMN Persona.nombre IS 'Nombre/s de la persona.';
COMMENT ON COLUMN Persona.apellido IS 'Apellido/s de la persona.';
COMMENT ON COLUMN Persona.lugar_nacimiento IS 'Referencia (fk) a la entidad ciudad.';
COMMENT ON COLUMN Persona.sexo_id IS 'Referencia (fk) a la entidad sexo.';
COMMENT ON COLUMN Persona.nacionalidad_id IS 'Referencia (fk) a la entidad nacionalidad.';
COMMENT ON COLUMN Persona.direccion IS 'Direcci�n de domicilio particular actual.';
COMMENT ON COLUMN Persona.telefono IS 'N�mero telef�nico particular actual.';
COMMENT ON COLUMN Persona.email IS 'Direcci�n de correo electr�nico particular actual.';
COMMENT ON COLUMN Persona.activo IS 'Estado de la entidad.';


CREATE UNIQUE INDEX persona_nro_documento_uq
 ON Persona USING BTREE
 ( numero_documento ASC );

CREATE TABLE Usuario (
                id INTEGER NOT NULL,
                nombre VARCHAR(16) NOT NULL,
                clave VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                persona_id INTEGER NOT NULL,
                rol_id SMALLINT NOT NULL,
                Dep_id INTEGER NOT NULL,
                CONSTRAINT usuario_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Usuario IS 'Tabla para registrar los usuarios del sistema.';
COMMENT ON COLUMN Usuario.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN Usuario.nombre IS 'Nombre �nico de usuario.';
COMMENT ON COLUMN Usuario.clave IS 'Valor codificado MD5 de la contrase�a de acceso al sistema.';
COMMENT ON COLUMN Usuario.activo IS 'Estado de la entidad.';


CREATE UNIQUE INDEX usuario_nombre_uq
 ON Usuario USING BTREE
 ( nombre ASC );

CREATE TABLE Cliente (
                id INTEGER NOT NULL,
                persona_id INTEGER NOT NULL,
                Ticket_id INTEGER NOT NULL,
                Dep_id INTEGER NOT NULL,
                ruc VARCHAR(20),
                razon_social VARCHAR(128) NOT NULL,
                ciudad_id SMALLINT NOT NULL,
                CONSTRAINT cliente_pk PRIMARY KEY (id)
);
COMMENT ON TABLE Cliente IS 'Datos de Clientes.';
COMMENT ON COLUMN Cliente.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN Cliente.persona_id IS 'Referencia (fk) a la entidad persona.';
COMMENT ON COLUMN Cliente.ruc IS 'N�mero del registro �nico del contribuyente.';
COMMENT ON COLUMN Cliente.razon_social IS 'Nombre de raz�n social del cliente.';
COMMENT ON COLUMN Cliente.ciudad_id IS 'Referencia (fk) a la entidad distrito. Ciudad de su domicilio.';


ALTER TABLE Servicio ADD CONSTRAINT prioridad_servicio_fk
FOREIGN KEY (Prioridad_id)
REFERENCES Prioridad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Ticket ADD CONSTRAINT prioridad_ticket_fk
FOREIGN KEY (Prioridad_id)
REFERENCES Prioridad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Ticket ADD CONSTRAINT servicio_ticket_fk
FOREIGN KEY (Servicio_id)
REFERENCES Servicio (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Usuario ADD CONSTRAINT dep_usuario_fk
FOREIGN KEY (Dep_id)
REFERENCES Departamento_1 (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Cliente ADD CONSTRAINT departamento_clientes_fk
FOREIGN KEY (Dep_id)
REFERENCES Departamento_1 (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Ticket ADD CONSTRAINT departamento_ticket_fk
FOREIGN KEY (Dep_id)
REFERENCES Departamento_1 (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Puesto ADD CONSTRAINT departamento_puesto_fk
FOREIGN KEY (Dep_id)
REFERENCES Departamento_1 (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Ticket ADD CONSTRAINT puesto_ticket_fk
FOREIGN KEY (Puesto_id)
REFERENCES Puesto (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Cliente ADD CONSTRAINT ticket_clientes_fk
FOREIGN KEY (Ticket_id)
REFERENCES Ticket (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Asignacion ADD CONSTRAINT permiso_rol_permiso_fk
FOREIGN KEY (rol_id)
REFERENCES Permiso (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Asignacion ADD CONSTRAINT permiso_rol_permiso_fk1
FOREIGN KEY (permiso_id)
REFERENCES Permiso (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Asignacion ADD CONSTRAINT rol_rol_permiso_fk
FOREIGN KEY (rol_id)
REFERENCES Rol (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Usuario ADD CONSTRAINT rol_usuario_fk
FOREIGN KEY (rol_id)
REFERENCES Rol (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Departamento ADD CONSTRAINT pais_departamento_fk
FOREIGN KEY (pais_is)
REFERENCES Pais (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Nacionalidad ADD CONSTRAINT pais_nacionalidad_fk
FOREIGN KEY (pais_id)
REFERENCES Pais (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Persona ADD CONSTRAINT nacionalidad_persona_fk
FOREIGN KEY (nacionalidad_id)
REFERENCES Nacionalidad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Persona ADD CONSTRAINT sexo_persona_fk
FOREIGN KEY (sexo_id)
REFERENCES Sexo (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Persona ADD CONSTRAINT tipo_documento_persona_fk
FOREIGN KEY (tipo_documento_id)
REFERENCES Tipo_documento (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Persona ADD CONSTRAINT tipo_persona_persona_fk
FOREIGN KEY (tipo_persona_id)
REFERENCES Tipo_persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Ciudad ADD CONSTRAINT departamento_distrito_fk
FOREIGN KEY (departamento_id)
REFERENCES Departamento (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Persona ADD CONSTRAINT distrito_persona_fk
FOREIGN KEY (lugar_nacimiento)
REFERENCES Ciudad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Cliente ADD CONSTRAINT ciudad_clientes_fk
FOREIGN KEY (ciudad_id)
REFERENCES Ciudad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Cliente ADD CONSTRAINT persona_cliente_fk
FOREIGN KEY (persona_id)
REFERENCES Persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Usuario ADD CONSTRAINT persona_usuario_fk
FOREIGN KEY (id)
REFERENCES Persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
